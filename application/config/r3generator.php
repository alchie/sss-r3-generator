<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('APP_NAME')  OR define('APP_NAME', 'SSS R3 Online Generator');
defined('APP_VERSION')  OR define('APP_VERSION', '1.0.0');

$config['app_name'] = 'SSS R3 Online Generator';