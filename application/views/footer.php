<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

</div> <!-- #bodyWrapper -->

<div class="container hide-print">
	<div class="row">
		<div class="col-md-6">
			<small>

<?php if( $this->config->item('multi_company') ) { ?>
Current <?php echo lang_term('companies_title_singular', 'Company'); ?>: <strong>
<a class="ajax-modal" href="#ajaxModal" data-hide_footer="1" data-toggle="modal" data-target="#ajaxModal" data-title="Switch Company" data-url="<?php echo site_url("welcome/select_company/0/ajax") . "?next=" . uri_string(); ?>">
      <?php echo $this->session->userdata( 'current_company' ); ?></a></strong>
<?php } ?>
				<p><a href="http://r3gen.chesteralan.com/" target="footer_credits"><?php echo APP_NAME; ?> v<?php echo APP_VERSION; ?></a> 
				&copy; <?php echo date('Y'); ?> </p>
				
				
			</small>
		</div>
    <div class="col-md-6 text-right">
<small>
Developed by: 
        <a href="http://www.chesteralan.com/" target="footer_credits">Chester Alan Tagudin</a>
        <p><strong>M:</strong> <?php echo $this->benchmark->memory_usage();?> <strong>T:</strong> <?php echo $this->benchmark->elapsed_time();?> <strong>IP:</strong> <?php echo $this->input->ip_address(); ?></p>
</small>
    </div> 
	</div> 
</div>

    <script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap-4.0.0/dist/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/r3generator.js'); ?>"></script>


  </body>
</html>
