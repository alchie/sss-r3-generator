<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('employers/dashboard_topnav'); ?>

    <div class="container-fluid">
      <div class="row">

      <?php $this->load->view('employers/dashboard_sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

<a href="<?php echo site_url("employers/add_payment/{$employer->sss_id}"); ?>" class="btn btn-sm btn-success float-right">Add Payment</a>

          <h2>R5 Payments 
            <span class="badge badge-success"><?php echo $employer->payments_count; ?></span>
            <span class="badge badge-success"><?php echo number_format($employer->payments_total,2); ?></span>
          </h2>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>Date Paid</th>
                  <th>OR / SBR Number</th>
                  <th>Coverage</th>
                  <th>Amount Paid</th>
                  <th>Applied</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
<?php foreach($payments as $payment) { ?>
                <tr>
                  <td><?php echo $payment->date_paid; ?></td>
                  <td><?php echo $payment->or_number; ?></td>
                  <td><?php echo $payment->coverage; ?></td>
                  <td><?php echo number_format($payment->amount,2); ?></td>
                  <td><?php echo number_format($payment->applied,2); ?></td>
                  <td class="text-right" width="150px"><a href="<?php echo site_url("employers/edit_payment/{$payment->id}"); ?>" class="btn btn-sm btn-warning">Edit</a> <a href="<?php echo site_url("employers/r3_form/{$payment->id}"); ?>" class="btn btn-sm btn-danger">R3 Form</a></td>
                </tr>
<?php } ?>
          
              </tbody>
            </table>
          </div>

<?php echo $pagination; ?>
        </main>



      </div>
    </div>
