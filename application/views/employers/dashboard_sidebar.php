        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url("employers/dashboard/{$employer->sss_id}"); ?>">
                  <span data-feather="home"></span>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url("employers/employees/{$employer->sss_id}"); ?>">
                  <span data-feather="file"></span>
                  Employees
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url("employers/r5payments/{$employer->sss_id}"); ?>">
                  <span data-feather="shopping-cart"></span>
                  R5 Payments 
                </a>
              </li>

            </ul>

          </div>



        </nav>