<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('employers/dashboard_topnav'); ?>

    <div class="container-fluid">
      <div class="row">

      <?php $this->load->view('employers/dashboard_sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

          <h2>Edit Employee</h2>
         

        <div class="container">

          <div class="row">
            <div class="col-md-6">
            	
<form method="post">

  <div class="form-group">
    <label>SSS Number</label>
    <input name="sss_number" type="text" class="form-control" placeholder="SSS Number" value="<?php echo $employee->sss_number; ?>">
  </div>

  <div class="form-group">
    <label>Last Name</label>
    <input name="last_name" type="text" class="form-control" placeholder="Last Name" value="<?php echo $employee->lastname; ?>">
  </div>

  <div class="form-group">
    <label>First Name</label>
    <input name="first_name" type="text" class="form-control" placeholder="First Name" value="<?php echo $employee->firstname; ?>">
  </div>

  <div class="form-group">
    <label>Middle Name</label>
    <input name="middle_name" type="text" class="form-control" placeholder="Middle Name" value="<?php echo $employee->middlename; ?>">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

            </div>

          </div>
        </div>

        </main>



      </div>
    </div>
