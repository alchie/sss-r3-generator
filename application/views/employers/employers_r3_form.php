<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('employers/dashboard_topnav'); ?>

    <div class="container-fluid">
      <div class="row">

      <?php $this->load->view('employers/dashboard_sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
<form method="post">
          <a href="<?php echo site_url("employers/add_r3_employee/{$payment->id}"); ?>" class="btn btn-sm btn-success float-right">Add Employee</a>
          <button type="submit" class="btn btn-sm btn-danger float-right" style="margin-right: 10px;">Save Changes</button>
          <h2>R3 Form 

              <span class="badge badge-success"><?php echo $payment->date_paid; ?></span>
              <span class="badge badge-success"><?php echo $payment->or_number; ?></span>
              <span class="badge badge-success"><?php echo $payment->coverage; ?></span>
          </h2>
          <div class="table-responsive">


            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th width="1px"></th>
                  <th>SSS #</th>
                  <th>Lastname</th>
                  <th>Firstname</th>
                  <th>Middlename</th>
                  <th width="150px">SS</th>
                  <th width="150px">EC</th>
                  <th width="150px">TOTAL</th>
                </tr>
              </thead>
              <tbody>
<?php 
$total_ss = 0;
$total_ec = 0;
foreach($employees as $employee) { ?>
                <tr>
                  <td><a href="<?php echo site_url("employers/remove_r3_employee/{$payment->id}/{$employee->r3id}"); ?>"><span class="fa fa-remove" style="color:red;"></span></a></td>
                  <td><?php echo $employee->sss_number; ?></td>
                  <td><?php echo $employee->lastname; ?></td>
                  <td><?php echo $employee->firstname; ?></td>
                  <td><?php echo $employee->middlename; ?></td>
                  <td><input name="ss[<?php echo $employee->r3id; ?>]" type="text" class="form-control text-right" value="<?php echo $employee->ss; $total_ss+=$employee->ss; ?>"></td>
                  <td><input name="ec[<?php echo $employee->r3id; ?>]" btype="text" class="form-control text-right" value="<?php echo ($employee->ec) ? $employee->ec : '10.00'; $total_ec+=$employee->ec; ?>"></td>
                  <td><strong class=" text-right form-control"><?php echo number_format( ($employee->ss + $employee->ec), 2 ); ?></strong></td>
                  
                </tr>
<?php } ?>
<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>TOTAL APPLIED</strong></td>
                  <td class="text-right"><strong class=" form-control"><?php echo number_format($total_ss,2); ?></strong></td>
                  <td class="text-right"><strong class=" form-control"><?php echo number_format($total_ec,2); ?></strong></td>
                  <td class="text-right"><strong class=" form-control"><?php echo number_format(($total_ss+$total_ec),2); ?></strong></td>
                </tr>
<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>TOTAL PAYMENT</strong></td>
                  <td></td>
                  <td></td>
                  <td class="text-right"><strong class=" form-control"><?php echo number_format($payment->amount,2); ?></strong></td>
                </tr>
<?php $balance = $payment->amount - ($total_ss+$total_ec); ?>
<?php if($balance <> 0) { ?>
<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>BALANCE</strong></td>
                  <td></td>
                  <td></td>

                  <td class="text-right"><strong class=" form-control" style="color: <?php echo ($balance <> 0) ? 'red' : ''; ?>"><?php echo number_format($balance,2); ?></strong></td>
                </tr>
<?php } else { ?>
<tr>
                  <td colspan="8">
<?php if( !$payment->filename ) { ?>
                     <a href="<?php echo site_url("employers/create_filename/{$payment->id}"); ?>" class="btn btn-sm btn-danger">Create File Name</a>
<?php } else { ?>
  <p>
Filename: <strong><?php echo $payment->filename; ?></strong><br>
    <a href="<?php echo site_url("employers/remove_filename/{$payment->id}"); ?>" class="btn btn-sm btn-danger"><span class="fa fa-remove"></span> Remove File Name</a>  <a class="btn btn-sm btn-success" href="<?php echo site_url("employers/download_r3_file/{$payment->id}"); ?>"><span class="fa fa-download"></span> Download R3 File</a></p>

            <a class="btn btn-sm btn-success" href="<?php echo site_url("employers/r3_print/{$payment->id}"); ?>"> <span class="fa fa-print"></span> Print Transmittal & Employee List</a>

            <br>Print 2 copies
<?php } ?>

                  </td>
                  
                </tr>
<?php } ?>
              </tbody>
            </table>

          </div>


</form>
        </main>


      </div>
    </div>
