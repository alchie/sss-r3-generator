<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('employers/dashboard_topnav'); ?>

    <div class="container-fluid">
      <div class="row">

      <?php $this->load->view('employers/dashboard_sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

          <h2><?php echo $employer->name; ?> <span class="badge badge-success">SSS Employer ID: <?php echo $employer->sss_id; ?></span></h2>
         
<p>
	<strong>Employees:</strong> <?php echo $employer->employees_count; ?>
	<br>
	<strong>R5 Payments:</strong> <?php echo $employer->payments_count; ?>
</p>

        </main>



      </div>
    </div>
