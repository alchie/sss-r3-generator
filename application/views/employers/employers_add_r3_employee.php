<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('employers/dashboard_topnav'); ?>

    <div class="container-fluid">
      <div class="row">

      <?php $this->load->view('employers/dashboard_sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <a href="<?php echo site_url("employers/r3_form/{$payment->id}"); ?>" class="btn btn-sm btn-success float-right">Back to R3 Form</a>
          <h2>Employees</h2>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>SSS #</th>
                  <th>Lastname</th>
                  <th>Firstname</th>
                  <th>Middlename</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
<?php foreach($employees as $employee) { ?>
                <tr>
                  <td><?php echo $employee->sss_number; ?></td>
                  <td><?php echo $employee->lastname; ?></td>
                  <td><?php echo $employee->firstname; ?></td>
                  <td><?php echo $employee->middlename; ?></td>
                  <td class="text-right"><a href="<?php echo site_url("employers/add_r3_employee/{$payment->id}") . "?add=" . $employee->id; ?>" class="btn btn-sm btn-info">Add to Payment</a></td>
                </tr>
<?php } ?>

              </tbody>
            </table>
          </div>


        </main>



      </div>
    </div>
