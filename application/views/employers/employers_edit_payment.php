<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('employers/dashboard_topnav'); ?>

    <div class="container-fluid">
      <div class="row">

      <?php $this->load->view('employers/dashboard_sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

          <h2>Edit R5 Payment</h2>
         
        <div class="container">

          <div class="row">
            <div class="col-md-6">

<form method="post">

  <div class="form-group">
    <label>Date Paid</label>
    <input name="date_paid" type="text" class="form-control" placeholder="Date Paid" value="<?php echo date("m/d/Y", strtotime( $payment->date_paid )); ?>">
  </div>

  <div class="form-group">
    <label>OR / SBR Number</label>
    <input name="or_number" type="text" class="form-control" placeholder="OR / SBR Number"  value="<?php echo $payment->or_number; ?>">
  </div>

  <div class="form-group">
    <label>Coverage</label>
    <input name="coverage" type="text" class="form-control" placeholder="Coverage" value="<?php echo $payment->coverage; ?>">
  </div>

  <div class="form-group">
    <label>Amount</label>
    <input name="amount" type="text" class="form-control" placeholder="Amount" value="<?php echo $payment->amount; ?>">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

</div>
</div>
</div>

        </main>



      </div>
    </div>
