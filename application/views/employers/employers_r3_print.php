<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo (isset($page_title)) ? $page_title : APP_NAME; ?></title>
    <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">

<style>
  body {
    font-size: 13px;
    font-family: "Courier New";
  }
</style>
  </head>
  <body>
<?php 

$employer_name = substr(strtoupper($employer->name), 0, 30);
$employer_sss_id = str_replace("-", "", $employer->sss_id );

$coverage = str_replace("-", "", $payment->coverage );
$date_paid = str_replace("-", "", date("m-d-Y", strtotime($payment->date_paid)) );
$amount_paid = str_pad($payment->amount, 12, 0, STR_PAD_LEFT);
$or_number = substr(strtoupper($payment->or_number), 0, 10);
$final_date = date("M d, Y", strtotime($payment->finalized));
$total_ec = 0;
$total_ss = 0;

$total_ec = 0;
$total_ss = 0;
$count_employees = 0;
foreach($employees as $employee) {
  $total_ss += $employee->ss;
  $total_ec += $employee->ec;
  $count_employees++;
}

?>
<div class="page">
<br><br>
File&nbsp;name&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo $payment->filename; ?><br>
Employer&nbsp;name:&nbsp;<?php echo $employer_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;Date:&nbsp;<?php echo $final_date; ?><br>
Employer&nbsp;No&nbsp;&nbsp;:&nbsp;<?php echo $employer->sss_id; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;App.&nbsp;Period:&nbsp;<?php echo $coverage; ?><br>
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PAYMENT&nbsp;INFORMATION<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TR/SBR&nbsp;NUMBER&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $payment->or_number; ?><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATE&nbsp;OF&nbsp;PAYMENT&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("M d, Y", strtotime($payment->date_paid)); ?><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AMOUNT&nbsp;PAID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($payment->amount,2); ?><br>
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENCODED&nbsp;INFORMATION<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SS&nbsp;AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($total_ss,2); ?><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EC&nbsp;AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($total_ec,2); ?><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOTAL&nbsp;AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo number_format($total_ec+$total_ss,2); ?><br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;number&nbsp;of&nbsp;Employees:&nbsp;<?php echo $count_employees; ?><br>
<br>
<br>
<br>
CERTIFIED&nbsp;CORRECT&nbsp;AND&nbsp;PAID<br>
RECEIVED&nbsp;BY&nbsp;&nbsp;&nbsp;:&nbsp;_____________________________<br>
DATE&nbsp;RECEIVED&nbsp;:&nbsp;_____________________________<br>
TRANSACTION&nbsp;NO:&nbsp;_____________________________<br>

</div>
<div&nbsp;class="page">
<br><br>
<?php echo $employer_name; ?>&nbsp;[<?php echo $payment->filename; ?>]<br>
Date:&nbsp;<?php echo $final_date; ?><br>
<br>
FAMILY&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GIVEN&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MI&nbsp;&nbsp;SS&nbsp;NUMBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S.S.&nbsp;&nbsp;&nbsp;&nbsp;E.C.&nbsp;&nbsp;&nbsp;RMRK&nbsp;&nbsp;DTHRD<br>
<br>
<?php foreach($employees as $employee) { 

$lastname = str_replace(" ", "&nbsp;", str_pad(strtoupper($employee->lastname), 16));
$firstname = str_replace(" ", "&nbsp;", str_pad(strtoupper($employee->firstname), 16));
$middlename = str_replace(" ", "&nbsp;", str_pad(substr(strtoupper($employee->middlename),0,1), 4));
$sss_number = str_replace(" ", "&nbsp;", str_pad(strtoupper($employee->sss_number), 16));
$ss = str_replace(" ", "&nbsp;", str_pad($employee->ss, 8));
$ec = str_replace(" ", "&nbsp;", str_pad($employee->ec, 8));

  ?>
<?php echo $lastname; ?><?php echo $firstname; ?><?php echo $middlename; ?><?php echo $sss_number; ?><?php echo $ss; ?><?php echo $ec; ?>N&nbsp;&nbsp;&nbsp;0<br>
<?php } ?>
<br>
<br>
Total&nbsp;Number&nbsp;of&nbsp;Employees:&nbsp;<?php echo $count_employees; ?><br>


</div>
  </body>
</html>
