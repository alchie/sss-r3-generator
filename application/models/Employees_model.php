<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Employees_model Class
 *
 * Manipulates `employees` table on database

CREATE TABLE `employees` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `employer_id` varchar(20) NOT NULL,
  `sss_number` varchar(20) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin;

ALTER TABLE  `employees` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `employees` ADD  `employer_id` varchar(20) NOT NULL   ;
ALTER TABLE  `employees` ADD  `sss_number` varchar(20) NOT NULL   ;
ALTER TABLE  `employees` ADD  `lastname` varchar(100) NOT NULL   ;
ALTER TABLE  `employees` ADD  `firstname` varchar(100) NOT NULL   ;
ALTER TABLE  `employees` ADD  `middlename` varchar(100) NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Employees_model extends MY_Model {

	protected $id;
	protected $employer_id;
	protected $sss_number;
	protected $lastname;
	protected $firstname;
	protected $middlename;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'employees';
		$this->_short_name = 'employees';
		$this->_fields = array("id","employer_id","sss_number","lastname","firstname","middlename");
		$this->_required = array("employer_id","sss_number","lastname","firstname","middlename");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: employer_id -------------------------------------- 

	/** 
	* Sets a value to `employer_id` variable
	* @access public
	*/

	public function setEmployerId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('employer_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_employer_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('employer_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `employer_id` variable
	* @access public
	*/

	public function getEmployerId() {
		return $this->employer_id;
	}

	public function get_employer_id() {
		return $this->employer_id;
	}

	
// ------------------------------ End Field: employer_id --------------------------------------


// ---------------------------- Start Field: sss_number -------------------------------------- 

	/** 
	* Sets a value to `sss_number` variable
	* @access public
	*/

	public function setSssNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('sss_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_sss_number($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('sss_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `sss_number` variable
	* @access public
	*/

	public function getSssNumber() {
		return $this->sss_number;
	}

	public function get_sss_number() {
		return $this->sss_number;
	}

	
// ------------------------------ End Field: sss_number --------------------------------------


// ---------------------------- Start Field: lastname -------------------------------------- 

	/** 
	* Sets a value to `lastname` variable
	* @access public
	*/

	public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_lastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `lastname` variable
	* @access public
	*/

	public function getLastname() {
		return $this->lastname;
	}

	public function get_lastname() {
		return $this->lastname;
	}

	
// ------------------------------ End Field: lastname --------------------------------------


// ---------------------------- Start Field: firstname -------------------------------------- 

	/** 
	* Sets a value to `firstname` variable
	* @access public
	*/

	public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('firstname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_firstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('firstname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `firstname` variable
	* @access public
	*/

	public function getFirstname() {
		return $this->firstname;
	}

	public function get_firstname() {
		return $this->firstname;
	}

	
// ------------------------------ End Field: firstname --------------------------------------


// ---------------------------- Start Field: middlename -------------------------------------- 

	/** 
	* Sets a value to `middlename` variable
	* @access public
	*/

	public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('middlename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_middlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('middlename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `middlename` variable
	* @access public
	*/

	public function getMiddlename() {
		return $this->middlename;
	}

	public function get_middlename() {
		return $this->middlename;
	}

	
// ------------------------------ End Field: middlename --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'employer_id' => (object) array(
										'Field'=>'employer_id',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'sss_number' => (object) array(
										'Field'=>'sss_number',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'lastname' => (object) array(
										'Field'=>'lastname',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'firstname' => (object) array(
										'Field'=>'firstname',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'middlename' => (object) array(
										'Field'=>'middlename',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `employees` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'employer_id' => "ALTER TABLE  `employees` ADD  `employer_id` varchar(20) NOT NULL   ;",
			'sss_number' => "ALTER TABLE  `employees` ADD  `sss_number` varchar(20) NOT NULL   ;",
			'lastname' => "ALTER TABLE  `employees` ADD  `lastname` varchar(100) NOT NULL   ;",
			'firstname' => "ALTER TABLE  `employees` ADD  `firstname` varchar(100) NOT NULL   ;",
			'middlename' => "ALTER TABLE  `employees` ADD  `middlename` varchar(100) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setEmployerId() - employer_id
//setSssNumber() - sss_number
//setLastname() - lastname
//setFirstname() - firstname
//setMiddlename() - middlename

--------------------------------------

//set_id() - id
//set_employer_id() - employer_id
//set_sss_number() - sss_number
//set_lastname() - lastname
//set_firstname() - firstname
//set_middlename() - middlename

*/
/* End of file Employees_model.php */
/* Location: ./application/models/Employees_model.php */
