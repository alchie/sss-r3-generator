CREATE TABLE `employees` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `employer_id` varchar(20) NOT NULL,
  `sss_number` varchar(20) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin;

-- Table structure for table `employees` 

CREATE TABLE `employers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sss_id` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `sssid` (`sss_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

-- Table structure for table `employers` 

CREATE TABLE `r3forms` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `payment_id` int(20) NOT NULL,
  `employee_id` int(20) NOT NULL,
  `ss` decimal(10,2) NOT NULL,
  `ec` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=latin;

-- Table structure for table `r3forms` 

CREATE TABLE `r5payments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `employer_id` varchar(20) NOT NULL,
  `date_paid` date NOT NULL,
  `or_number` varchar(20) NOT NULL,
  `coverage` varchar(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `finalized` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin;

-- Table structure for table `r5payments` 

