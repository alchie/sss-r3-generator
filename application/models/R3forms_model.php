<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * R3forms_model Class
 *
 * Manipulates `r3forms` table on database

CREATE TABLE `r3forms` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `payment_id` int(20) NOT NULL,
  `employee_id` int(20) NOT NULL,
  `ss` decimal(10,2) NOT NULL,
  `ec` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=latin;

ALTER TABLE  `r3forms` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `r3forms` ADD  `payment_id` int(20) NOT NULL   ;
ALTER TABLE  `r3forms` ADD  `employee_id` int(20) NOT NULL   ;
ALTER TABLE  `r3forms` ADD  `ss` decimal(10,2) NOT NULL   ;
ALTER TABLE  `r3forms` ADD  `ec` decimal(10,2) NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class R3forms_model extends MY_Model {

	protected $id;
	protected $payment_id;
	protected $employee_id;
	protected $ss;
	protected $ec;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'r3forms';
		$this->_short_name = 'r3forms';
		$this->_fields = array("id","payment_id","employee_id","ss","ec");
		$this->_required = array("payment_id","employee_id","ss","ec");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: payment_id -------------------------------------- 

	/** 
	* Sets a value to `payment_id` variable
	* @access public
	*/

	public function setPaymentId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('payment_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_payment_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('payment_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `payment_id` variable
	* @access public
	*/

	public function getPaymentId() {
		return $this->payment_id;
	}

	public function get_payment_id() {
		return $this->payment_id;
	}

	
// ------------------------------ End Field: payment_id --------------------------------------


// ---------------------------- Start Field: employee_id -------------------------------------- 

	/** 
	* Sets a value to `employee_id` variable
	* @access public
	*/

	public function setEmployeeId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('employee_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_employee_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('employee_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `employee_id` variable
	* @access public
	*/

	public function getEmployeeId() {
		return $this->employee_id;
	}

	public function get_employee_id() {
		return $this->employee_id;
	}

	
// ------------------------------ End Field: employee_id --------------------------------------


// ---------------------------- Start Field: ss -------------------------------------- 

	/** 
	* Sets a value to `ss` variable
	* @access public
	*/

	public function setSs($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ss', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ss($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ss', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ss` variable
	* @access public
	*/

	public function getSs() {
		return $this->ss;
	}

	public function get_ss() {
		return $this->ss;
	}

	
// ------------------------------ End Field: ss --------------------------------------


// ---------------------------- Start Field: ec -------------------------------------- 

	/** 
	* Sets a value to `ec` variable
	* @access public
	*/

	public function setEc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ec', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ec($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ec', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ec` variable
	* @access public
	*/

	public function getEc() {
		return $this->ec;
	}

	public function get_ec() {
		return $this->ec;
	}

	
// ------------------------------ End Field: ec --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'payment_id' => (object) array(
										'Field'=>'payment_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'employee_id' => (object) array(
										'Field'=>'employee_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ss' => (object) array(
										'Field'=>'ss',
										'Type'=>'decimal(10,2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ec' => (object) array(
										'Field'=>'ec',
										'Type'=>'decimal(10,2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `r3forms` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'payment_id' => "ALTER TABLE  `r3forms` ADD  `payment_id` int(20) NOT NULL   ;",
			'employee_id' => "ALTER TABLE  `r3forms` ADD  `employee_id` int(20) NOT NULL   ;",
			'ss' => "ALTER TABLE  `r3forms` ADD  `ss` decimal(10,2) NOT NULL   ;",
			'ec' => "ALTER TABLE  `r3forms` ADD  `ec` decimal(10,2) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setPaymentId() - payment_id
//setEmployeeId() - employee_id
//setSs() - ss
//setEc() - ec

--------------------------------------

//set_id() - id
//set_payment_id() - payment_id
//set_employee_id() - employee_id
//set_ss() - ss
//set_ec() - ec

*/
/* End of file R3forms_model.php */
/* Location: ./application/models/R3forms_model.php */
