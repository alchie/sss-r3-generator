<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * R5payments_model Class
 *
 * Manipulates `r5payments` table on database

CREATE TABLE `r5payments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `employer_id` varchar(20) NOT NULL,
  `date_paid` date NOT NULL,
  `or_number` varchar(20) NOT NULL,
  `coverage` varchar(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `finalized` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin;

ALTER TABLE  `r5payments` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `r5payments` ADD  `employer_id` varchar(20) NOT NULL   ;
ALTER TABLE  `r5payments` ADD  `date_paid` date NOT NULL   ;
ALTER TABLE  `r5payments` ADD  `or_number` varchar(20) NOT NULL   ;
ALTER TABLE  `r5payments` ADD  `coverage` varchar(10) NOT NULL   ;
ALTER TABLE  `r5payments` ADD  `amount` decimal(10,2) NOT NULL   ;
ALTER TABLE  `r5payments` ADD  `filename` varchar(100) NULL   ;
ALTER TABLE  `r5payments` ADD  `finalized` datetime NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class R5payments_model extends MY_Model {

	protected $id;
	protected $employer_id;
	protected $date_paid;
	protected $or_number;
	protected $coverage;
	protected $amount;
	protected $filename;
	protected $finalized;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'r5payments';
		$this->_short_name = 'r5payments';
		$this->_fields = array("id","employer_id","date_paid","or_number","coverage","amount","filename","finalized");
		$this->_required = array("employer_id","date_paid","or_number","coverage","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: employer_id -------------------------------------- 

	/** 
	* Sets a value to `employer_id` variable
	* @access public
	*/

	public function setEmployerId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('employer_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_employer_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('employer_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `employer_id` variable
	* @access public
	*/

	public function getEmployerId() {
		return $this->employer_id;
	}

	public function get_employer_id() {
		return $this->employer_id;
	}

	
// ------------------------------ End Field: employer_id --------------------------------------


// ---------------------------- Start Field: date_paid -------------------------------------- 

	/** 
	* Sets a value to `date_paid` variable
	* @access public
	*/

	public function setDatePaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('date_paid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_date_paid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('date_paid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `date_paid` variable
	* @access public
	*/

	public function getDatePaid() {
		return $this->date_paid;
	}

	public function get_date_paid() {
		return $this->date_paid;
	}

	
// ------------------------------ End Field: date_paid --------------------------------------


// ---------------------------- Start Field: or_number -------------------------------------- 

	/** 
	* Sets a value to `or_number` variable
	* @access public
	*/

	public function setOrNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('or_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_or_number($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('or_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `or_number` variable
	* @access public
	*/

	public function getOrNumber() {
		return $this->or_number;
	}

	public function get_or_number() {
		return $this->or_number;
	}

	
// ------------------------------ End Field: or_number --------------------------------------


// ---------------------------- Start Field: coverage -------------------------------------- 

	/** 
	* Sets a value to `coverage` variable
	* @access public
	*/

	public function setCoverage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('coverage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_coverage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('coverage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `coverage` variable
	* @access public
	*/

	public function getCoverage() {
		return $this->coverage;
	}

	public function get_coverage() {
		return $this->coverage;
	}

	
// ------------------------------ End Field: coverage --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->amount;
	}

	public function get_amount() {
		return $this->amount;
	}

	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: filename -------------------------------------- 

	/** 
	* Sets a value to `filename` variable
	* @access public
	*/

	public function setFilename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('filename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_filename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('filename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `filename` variable
	* @access public
	*/

	public function getFilename() {
		return $this->filename;
	}

	public function get_filename() {
		return $this->filename;
	}

	
// ------------------------------ End Field: filename --------------------------------------


// ---------------------------- Start Field: finalized -------------------------------------- 

	/** 
	* Sets a value to `finalized` variable
	* @access public
	*/

	public function setFinalized($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('finalized', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_finalized($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('finalized', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `finalized` variable
	* @access public
	*/

	public function getFinalized() {
		return $this->finalized;
	}

	public function get_finalized() {
		return $this->finalized;
	}

	
// ------------------------------ End Field: finalized --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'employer_id' => (object) array(
										'Field'=>'employer_id',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'date_paid' => (object) array(
										'Field'=>'date_paid',
										'Type'=>'date',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'or_number' => (object) array(
										'Field'=>'or_number',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'coverage' => (object) array(
										'Field'=>'coverage',
										'Type'=>'varchar(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'amount' => (object) array(
										'Field'=>'amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'filename' => (object) array(
										'Field'=>'filename',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'finalized' => (object) array(
										'Field'=>'finalized',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `r5payments` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'employer_id' => "ALTER TABLE  `r5payments` ADD  `employer_id` varchar(20) NOT NULL   ;",
			'date_paid' => "ALTER TABLE  `r5payments` ADD  `date_paid` date NOT NULL   ;",
			'or_number' => "ALTER TABLE  `r5payments` ADD  `or_number` varchar(20) NOT NULL   ;",
			'coverage' => "ALTER TABLE  `r5payments` ADD  `coverage` varchar(10) NOT NULL   ;",
			'amount' => "ALTER TABLE  `r5payments` ADD  `amount` decimal(10,2) NOT NULL   ;",
			'filename' => "ALTER TABLE  `r5payments` ADD  `filename` varchar(100) NULL   ;",
			'finalized' => "ALTER TABLE  `r5payments` ADD  `finalized` datetime NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setEmployerId() - employer_id
//setDatePaid() - date_paid
//setOrNumber() - or_number
//setCoverage() - coverage
//setAmount() - amount
//setFilename() - filename
//setFinalized() - finalized

--------------------------------------

//set_id() - id
//set_employer_id() - employer_id
//set_date_paid() - date_paid
//set_or_number() - or_number
//set_coverage() - coverage
//set_amount() - amount
//set_filename() - filename
//set_finalized() - finalized

*/
/* End of file R5payments_model.php */
/* Location: ./application/models/R5payments_model.php */
