<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employers extends CI_Controller {

	public function index()
	{
		$employers = new $this->Employers_model();
		$employers->set_order('name', 'ASC');
		$this->template_data->set('employers', $employers->populate() );
		$this->load->view('employers/employers', $this->template_data->get_data());
	}

	public function add()
	{
		if( $this->input->post() ) {
			$employer = new $this->Employers_model();
			$employer->setName($this->input->post('employer_name'));
			$employer->setSssId($this->input->post('sss_number'),true);
			if( !$employer->nonEmpty() ) {
				$employer->insert();
			}
		}
		$this->load->view('employers/employers_add', $this->template_data->get_data());
	}

	public function edit($employer_id)
	{
		$employer = new $this->Employers_model();
		$employer->setId($employer_id,true);
		if( $this->input->post() ) {
			$employer->setName($this->input->post('employer_name'),false,true);
			$employer->setSssId($this->input->post('sss_number'),false,true);
			if( $employer->nonEmpty() ) {
				$employer->update();
				redirect("employers");
			}
		}
		$this->template_data->set('employer', $employer->get() );
		$this->load->view('employers/employers_edit', $this->template_data->get_data());
	}

	public function dashboard($employer_id)
	{
		
		$employer = new $this->Employers_model('er');
		$employer->setId($employer_id,true);
		if(  $employer->nonEmpty() ) {
			$employer->set_select('*');
			$employer->set_select('(SELECT COUNT(*) FROM employees ee WHERE ee.employer_id=er.sss_id) as employees_count');
			$employer->set_select('(SELECT COUNT(*) FROM r5payments r5 WHERE r5.employer_id=er.sss_id) as payments_count');
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$this->load->view('employers/employers_dashboard', $this->template_data->get_data());
	}

	public function employees($employer_id)
	{
		
		$employer = new $this->Employers_model('er');
		$employer->setSssId($employer_id,true);
		if(  $employer->nonEmpty() ) {
			$employer->set_select('*');
			$employer->set_select('(SELECT COUNT(*) FROM employees ee WHERE ee.employer_id=er.sss_id) as employees_count');
			$employer->set_select('(SELECT COUNT(*) FROM r5payments r5 WHERE r5.employer_id=er.sss_id) as payments_count');
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$employees = new $this->Employees_model();
		$employees->setEmployerId($employer_id,true);
		$employees->set_order('lastname', 'ASC');
		$employees->set_order('firstname', 'ASC');
		$employees->set_order('middlename', 'ASC');
		$this->template_data->set('employees', $employees->populate());

		$this->load->view('employers/employers_employees', $this->template_data->get_data());
	}

	public function add_employee($employer_id)
	{

		if( $this->input->post() ) {
			$employee = new $this->Employees_model();
			$employee->setEmployerId($employer_id,true);
			$employee->setSssNumber($this->input->post('sss_number'),true);
			$employee->setLastname($this->input->post('last_name'));
			$employee->setFirstname($this->input->post('first_name'));
			$employee->setMiddlename($this->input->post('middle_name'));
			if( !$employee->nonEmpty() ) {
				$employee->insert();
				redirect( site_url("employers/add_employee/{$employer_id}") . "?success=" . $employee->get_inserted_id()  );
			}
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		

		$this->load->view('employers/employers_add_employee', $this->template_data->get_data());
	}

	public function edit_employee($employee_id)
	{

		$employee = new $this->Employees_model();
		$employee->setId($employee_id,true);
		if( $this->input->post() ) {
			$employee->setSssNumber($this->input->post('sss_number'),false,true);
			$employee->setLastname($this->input->post('last_name'),false,true);
			$employee->setFirstname($this->input->post('first_name'),false,true);
			$employee->setMiddlename($this->input->post('middle_name'),false,true);
			if( $employee->nonEmpty() ) {
				$employee->update();
				redirect( site_url("employers/edit_employee/{$employee_id}") . "?success=1"  );
			}
		}

		if(  $employee->nonEmpty() ) {
			$employee_data = $employee->get_results();
			$this->template_data->set('employee', $employee_data);
		} else {
			redirect("employers");
		}
		
		$employer = new $this->Employers_model();
		$employer->setSssId($employee_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		

		$this->load->view('employers/employers_edit_employee', $this->template_data->get_data());
	}

	public function r5payments($employer_id, $start=0)
	{
		
		$employer = new $this->Employers_model('er');
		$employer->setSssId($employer_id,true);
		if(  $employer->nonEmpty() ) {
			$employer->set_select('*');
			$employer->set_select('(SELECT COUNT(*) FROM r5payments r5 WHERE r5.employer_id=er.sss_id) as payments_count');
			$employer->set_select('(SELECT SUM(r5.amount) FROM r5payments r5 WHERE r5.employer_id=er.sss_id) as payments_total');
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$payments = new $this->R5payments_model('r5');
		$payments->setEmployerId($employer_id,true);
		$payments->set_order('id', 'DESC');
		//$payments->set_order('coverage', 'DESC');
		//$payments->set_order('date_paid', 'DESC');
		$payments->set_start($start);

		$payments->set_select('*');
		$payments->set_select('(SELECT SUM(r3.ss + r3.ec) FROM r3forms r3 WHERE r3.payment_id=r5.id) as applied');

		$this->template_data->set('payments', $payments->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/employers/r5payments/{$employer_id}"),
			'total_rows' => $payments->count_all_results(),
			'per_page' => $payments->get_limit(),
		)));

		$this->load->view('employers/employers_r5payments', $this->template_data->get_data());
	}

	public function add_payment($employer_id)
	{

		if( $this->input->post() ) {
			$payment = new $this->R5payments_model();
			$payment->setEmployerId($employer_id,true);
			$payment->setDatePaid(date("Y-m-d", strtotime( $this->input->post('date_paid') )) );
			$payment->setOrNumber($this->input->post('or_number') );
			$payment->setCoverage($this->input->post('coverage'), true);
			$payment->setAmount( str_replace(",", "", $this->input->post('amount')) );
			if( !$payment->nonEmpty() ) {
				$payment->insert();
				redirect( site_url("employers/add_payment/{$employer_id}") . "?success=" . $payment->get_inserted_id()  );
			}
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$this->load->view('employers/employers_add_payment', $this->template_data->get_data());
	}

	public function edit_payment($payment_id)
	{

		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);

		if( $this->input->post() ) {
			$payment->setDatePaid(date("Y-m-d", strtotime( $this->input->post('date_paid') )), false,true);
			$payment->setOrNumber($this->input->post('or_number'), false,true );
			$payment->setCoverage($this->input->post('coverage'), false,true);
			$payment->setAmount( str_replace(",", "", $this->input->post('amount')), false,true );
			if( $payment->nonEmpty() ) {
				$payment->update();
				redirect( site_url("employers/edit_payment/{$payment_id}") . "?success=1"  );
			}
		}

		if(  $payment->nonEmpty() ) {
			$payment_data = $payment->get_results();
			$this->template_data->set('payment', $payment_data);
		} else {
			redirect("employers");
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($payment_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$this->load->view('employers/employers_edit_payment', $this->template_data->get_data());
	}

	public function r3_form($payment_id)
	{
		
		if( $this->input->post() ) {
			
			foreach( $this->input->post('ss') as $id=>$value ) {
				$r3 = new $this->R3forms_model();
				$r3->setId($id,true);
				if(  $r3->nonEmpty() ) {
					$r3->setSs($value,false,true);
					$r3->update();
				}
			}
			foreach( $this->input->post('ec') as $id=>$value ) {
				$r3 = new $this->R3forms_model();
				$r3->setId($id,true);
				if(  $r3->nonEmpty() ) {
					$r3->setEc($value,false,true);
					$r3->update();
				}
			}
		}

		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);

		if(  $payment->nonEmpty() ) {
			$payment_data = $payment->get_results();
			$this->template_data->set('payment', $payment_data);
		} else {
			redirect("employers");
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($payment_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$employees = new $this->R3forms_model();
		$employees->setPaymentId($payment_id,true);
		$employees->set_join('employees e', 'e.id=r3forms.employee_id');
		$employees->set_select('e.*');
		$employees->set_select('r3forms.*');
		$employees->set_select('r3forms.id as r3id');
		$this->template_data->set('employees', $employees->populate());

		$this->load->view('employers/employers_r3_form', $this->template_data->get_data());
	}

	public function add_r3_employee($payment_id)
	{
		
		if( $this->input->get('add') ) {
			$r3 = new $this->R3forms_model();
			$r3->setPaymentId($payment_id,true);
			$r3->setEmployeeId($this->input->get('add'),true);
			if(  !$r3->nonEmpty() ) {
				$r3->insert();
				//redirect("employers/r3_form/{$payment_id}");
			}
		}

		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);

		if(  $payment->nonEmpty() ) {
			$payment_data = $payment->get_results();
			$this->template_data->set('payment', $payment_data);
		} else {
			redirect("employers");
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($payment_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$employees = new $this->Employees_model();
		$employees->setEmployerId($payment_data->employer_id,true);
		$employees->set_order('lastname', 'ASC');
		$employees->set_order('firstname', 'ASC');
		$employees->set_order('middlename', 'ASC');
		$employees->set_where('((SELECT COUNT(*) FROM r3forms WHERE payment_id='.$payment_id.' AND employee_id=employees.id) = 0)');
		if(  $employees->nonEmpty() ) {
			$this->template_data->set('employees', $employees->populate());
		} else {
			redirect("employers/r3_form/{$payment_id}");
		}

		$this->load->view('employers/employers_add_r3_employee', $this->template_data->get_data());
	}

	public function remove_r3_employee($payment_id, $r3id) {
		$employees = new $this->R3forms_model();
		$employees->setId($r3id,true);
		$employees->setPaymentId($payment_id,true);
		$employees->delete();
		redirect("employers/r3_form/{$payment_id}");
	}

	public function download_r3_file($payment_id) {

		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);

		if(  $payment->nonEmpty() ) {
			$payment_data = $payment->get_results();
		} else {
			redirect("employers/r3_form/{$payment_id}");
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($payment_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$employer_data = $employer->get();
		} else {
			redirect("employers");
		}

		$employees = new $this->R3forms_model();
		$employees->setPaymentId($payment_id,true);
		$employees->set_join('employees e', 'e.id=r3forms.employee_id');
		$employees->set_select('e.*');
		$employees->set_select('r3forms.*');
		$employees->set_select('r3forms.id as r3id');
		$employees->set_order('e.lastname', 'ASC');
		$employees_data = $employees->populate();

		//print_r( $employer_data );
		//print_r( $payment_data );
		//print_r( $employees_data );

$employer_name = substr(strtoupper($employer_data->name), 0, 30);
$employer_sss_id = str_replace("-", "", $employer_data->sss_id );

$coverage = str_replace("-", "", $payment_data->coverage );
$coverage_arr = explode("-", $payment_data->coverage); 
$int_coverage = intval($coverage_arr[0]);
$date_paid = str_replace("-", "", date("m-d-Y", strtotime($payment_data->date_paid)) );
$amount_paid = str_pad($payment_data->amount, 12, 0, STR_PAD_LEFT);
$or_number = str_pad(substr(strtoupper($payment_data->or_number), 0, 10), 10);

$total_ec = 0;
$total_ss = 0;
$emptext = "";
foreach($employees_data as $employee) {
	$lastname = str_pad(strtoupper($employee->lastname), 15);
	$firstname = str_pad(strtoupper($employee->firstname), 15);
	$middlename = substr(strtoupper($employee->middlename),0,1);
	$sss_number = str_replace("-", "", strtoupper($employee->sss_number));
	$ss = str_pad($employee->ss, 8, " ", STR_PAD_LEFT);
	$total_ss += $employee->ss;
	$ec = str_pad($employee->ec, 6, " ", STR_PAD_LEFT);
	$total_ec += $employee->ec;


	switch($int_coverage) {
		case 1:
		case 4:
		case 7:
		case 10:
			$emptext .= "\n20{$lastname}{$firstname}{$middlename}{$sss_number}{$ss}    0.00    0.00  0.00  0.00  0.00{$ec}  0.00  0.00      N0       ";
		break;
		case 2:
		case 5:
		case 8:
		case 11:
			$emptext .= "\n20{$lastname}{$firstname}{$middlename}{$sss_number}    0.00{$ss}    0.00  0.00  0.00  0.00  0.00{$ec}  0.00      N0       ";
		break;
		case 3:
		case 6:
		case 9:
		case 12:
			$emptext .= "\n20{$lastname}{$firstname}{$middlename}{$sss_number}    0.00    0.00{$ss}  0.00  0.00  0.00  0.00  0.00{$ec}      N0       ";
		break;
	}
}

$total_ss = str_pad(number_format($total_ss,2,".",""), 12, " ", STR_PAD_LEFT);
$total_ec = str_pad(number_format($total_ec,2,".",""), 10, " ", STR_PAD_LEFT);

switch($int_coverage) {
	case 1:
	case 4:
	case 7:
	case 10:
$content = <<<TEXT
00{$employer_name}{$coverage}{$employer_sss_id}{$or_number}{$date_paid}{$amount_paid}{$emptext}
99{$total_ss}        0.00        0.00      0.00      0.00      0.00{$total_ec}      0.00      0.00                    

TEXT;
	break;
	case 2:
	case 5:
	case 8:
	case 11:
$content = <<<TEXT
00{$employer_name}{$coverage}{$employer_sss_id}{$or_number}{$date_paid}{$amount_paid}{$emptext}
99        0.00{$total_ss}        0.00      0.00      0.00      0.00      0.00{$total_ec}      0.00                    

TEXT;
	break;
	case 3:
	case 6:
	case 9:
	case 12:
$content = <<<TEXT
00{$employer_name}{$coverage}{$employer_sss_id}{$or_number}{$date_paid}{$amount_paid}{$emptext}
99        0.00        0.00{$total_ss}      0.00      0.00      0.00      0.00      0.00{$total_ec}                    

TEXT;
	break;
}

		$this->load->helper('download');
		force_download($payment_data->filename, $content);

	}

	public function create_filename($payment_id) {

		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);

		if(  $payment->nonEmpty() ) {
			$payment_data = $payment->get_results();
		} else {
			redirect("employers/r3_form/{$payment_id}");
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($payment_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$employer_data = $employer->get();
		} else {
			redirect("employers");
		}

		$employer_sss_id = str_replace("-", "", $employer_data->sss_id );
		$coverage = str_replace("-", "", $payment_data->coverage );

		$file_name = 'R3' . $employer_sss_id . $coverage . "." . date('mdHi');
		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);
		$payment->setFilename($file_name);
		$payment->setFinalized(date("Y-m-d H:i:s"));
		$payment->update();
		redirect("employers/r3_form/{$payment_id}");
	}

	public function remove_filename($payment_id) {
		$file_name = 'R3' . $employer_sss_id . $coverage . "." . date('mdHi');
		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);
		$payment->setFilename('');
		$payment->update();
		redirect("employers/r3_form/{$payment_id}");
	}

	public function r3_print($payment_id) {

		$payment = new $this->R5payments_model();
		$payment->setId($payment_id,true);

		if(  $payment->nonEmpty() ) {
			$payment_data = $payment->get_results();
			$this->template_data->set('payment', $payment_data);
		} else {
			redirect("employers");
		}

		$employer = new $this->Employers_model();
		$employer->setSssId($payment_data->employer_id,true);
		if(  $employer->nonEmpty() ) {
			$this->template_data->set('employer', $employer->get());
		} else {
			redirect("employers");
		}

		$employees = new $this->R3forms_model();
		$employees->setPaymentId($payment_id,true);
		$employees->set_join('employees e', 'e.id=r3forms.employee_id');
		$employees->set_select('e.*');
		$employees->set_select('r3forms.*');
		$employees->set_select('r3forms.id as r3id');
		$this->template_data->set('employees', $employees->populate());

		$this->load->view('employers/employers_r3_print', $this->template_data->get_data());

	}

}
